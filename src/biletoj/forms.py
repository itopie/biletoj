# -*- coding: UTF-8 -*-
#
# biletoj: simple ticketing system
#
#   src/biletoj/forms.py
#
# Copyright (C) 2013-2014   Vincent Berset <vberset@itopie.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from wtforms import (Form, TextField, TextAreaField, IntegerField,
                     BooleanField, SelectField, DateField, validators)


class Config(Form):
    default_status = SelectField(
        u'Statut par défaut',
        [validators.Required(u'Champ obligatoire')],
        coerce=int,
    )
    default_location = SelectField(
        u'Lieu par défaut',
        [validators.Required(u'Champ obligatoire')],
        coerce=int,
    )

class Level(Form):
    label = TextField(
        u'Label',
        [validators.Required(u'Champ obligatoire')]
    )
    value = IntegerField(
        u'Valeur',
    )
    is_active = BooleanField(
        u'Réparations actives',
    )


class Status(Form):
    label = TextField(
        u'Label',
        [validators.Required(u'Champ obligatoire')]
    )
    level = SelectField(
        u'Niveau',
        coerce=int
    )
    priority = IntegerField(
        u'Priorité',
    )


class Techie(Form):
    name = TextField(
        u'Nom',
        [validators.Length(min=2, max=50,
                           message=u'Doit contenir entre 2 et 50 caractères'),
         validators.Required(message=u'Champ obligatoire')
        ]
    )
    priority = IntegerField(
        u'Priorité',
        [validators.NumberRange(
            min=0, max=10,
            message=u'Doit être compris entre 0 et 10'
        ) ],
        default=0
    )
    is_active = BooleanField(
        u'Actif',
        default=True
    )


class Location(Form):
    name = TextField(
        u'Nom',
        [validators.Required(u'Champ obligatoire')]
    )
    weight = IntegerField(
        u'Poids',
        default=0,
    )


class Customer(Form):
    name = TextField(
        u'Nom',
        [validators.Length(min=2, max=100),
         validators.Required(message=u'Champ obligatoire')
        ],
        description=u'Nom Prénom'
    )
    phone1 = TextField(
        u'Téléphone 1',
        [validators.Required(message=u'Champ obligatoire')],
        description=u'022 123 45 67'
    )
    phone2 = TextField(u'Téléphone 2')
    email = TextField(u'Adresse email')
    address = TextAreaField(u'Adresse postale')
    notes = TextAreaField(u'Notes')


class Reparation(Form):
    r_id = IntegerField(
        u'N°',
        [validators.Optional()],
    )
    customer = SelectField(
        u'Client',
        [validators.Required(u'Champ obligatoire')],
        coerce=int
    )
    deadline = DateField(
        u'Échéance',
        [validators.Optional()],
        description=u'JJ/MM/AAAA',
        format=u'%d/%m/%Y'
    )
    location = SelectField(
        u'Lieu',
        [validators.Required(u'Champ obligatoire')],
        coerce=int
    )
    description = TextField(
        u'Description',
        [validators.Required(u'Champ obligatoire'),
         validators.Length(1, 300, u'300 caractères maximum'),
        ]
    )
    model = TextField(
        u'Modèle',
        [validators.Length(0,100, u'100 caractères maximum')]
    )
    accepted_by = SelectField(
        u'Accepté par',
        [validators.NumberRange(min=1, message=u'Champ obligatoire')],
        coerce=int
    )


class RepQuickEdit(Form):
    status_id = SelectField(
        u'Statut ',
        [validators.Optional()],
        coerce=int
    )
    techie_id = SelectField(
        u'Atribué à ',
        [validators.Optional()],
        coerce=int
    )
