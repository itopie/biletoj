# -*- coding: UTF-8 -*-
# biletoj: simple ticketing system
#
#   src/biletoj/models.py
#
# Copyright (C) 2013-2014   Vincent Berset <vberset@itopie.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from collections import OrderedDict

from flask import url_for
from flask.ext.sqlalchemy import SQLAlchemy

from sqlalchemy import event

from biletoj.core import app
from biletoj.utils import now


db = SQLAlchemy(app)


TG_REPARATION_INSERT = '''
CREATE TRIGGER tg_rep_insert AFTER INSERT ON reparations FOR EACH ROW
BEGIN
    INSERT INTO history (reparation_id, status_id, techie_id, location_id)
    VALUES (NEW._id, NEW.status_id, NEW.techie_id, NEW.location_id);
END;
'''


TG_REPARATION_UPDATE = '''
CREATE TRIGGER tg_rep_update AFTER UPDATE ON reparations FOR EACH ROW
BEGIN
    DECLARE new_status BOOLEAN;
    DECLARE new_techie BOOLEAN;
    DECLARE new_location BOOLEAN;
    SET new_status = NOT (NEW.status_id <=> OLD.status_id);
    SET new_techie = NOT (NEW.techie_id <=> OLD.techie_id);
    SET new_location = NOT (NEW.location_id <=> OLD.location_id);
    IF (new_status OR new_techie OR new_location) THEN
        INSERT INTO history (reparation_id, status_id, techie_id, location_id)
        VALUES (
            NEW._id,
            IF (new_status, NEW.status_id, NULL),
            IF (new_techie, NEW.techie_id, NULL),
            IF (new_location, NEW.location_id, NULL)
        );
    END IF;
END;
'''


class Config(db.Model):
    """
    """
    __tablename__ = 'config'

    _id = db.Column(db.Integer, primary_key=True)
    default_status_id = db.Column(db.Integer, db.ForeignKey('statuses._id'))
    default_location_id = db.Column(db.Integer, db.ForeignKey('locations._id'))

    default_status = db.relationship('Status')
    default_location = db.relationship('Location')

    __mapper_args__ = {
        'order_by': _id.desc(),
    }

    @staticmethod
    def get_instance():
        config = Config.query.first()
        if config is None:
            config = Config()
            db.session.add(config)
            db.session.commit()
        return config

    @staticmethod
    def get_dict():
        config = Config.get_instance()
        return {
            'default_status': config.default_status_id,
            'default_location': config.default_location_id,
        }


class Techie(db.Model):
    """
    """
    __tablename__ = 'techies'

    _id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    priority = db.Column(db.Integer, default=0)
    is_active = db.Column(db.Boolean, default=True)

    reparations = db.relationship(
        'Reparation', backref=db.backref('owner'), lazy='dynamic',
        primaryjoin='Techie._id == Reparation.techie_id',
    )

    __mapper_args__ = {
        'order_by': [priority, _id],
    }

    @staticmethod
    def query_choices():
        return db.session.query(Techie._id, Techie.name)

    def __init__(self, name, priority=0, is_active=True):
        self.name = name
        self.priority = priority
        self.is_active = is_active

    def __repr__(self):
        return "<Techie('%s')>" % self.name

    def get_absolute_url(self):
        return url_for('techie', techie_id=self._id)


class Customer(db.Model):
    """
    """
    __tablename__ = 'customers'

    _id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    phone1 = db.Column(db.String(20))
    phone2 = db.Column(db.String(20))
    email = db.Column(db.String(100))
    address = db.Column(db.Text)
    notes = db.Column(db.Text)

    reparations = db.relationship(
        'Reparation',
        backref=db.backref('customer'), cascade='all',
        lazy='dynamic',
    )

    __mapper_args__ = {
        'order_by': db.func.lower(name),
    }

    @staticmethod
    def query_choices():
        return db.session.query(Customer._id, Customer.name) \
                         .order_by(db.func.lower(Customer.name))

    def __init__(self, name, phone1):
        self.name = name
        self.phone1 = phone1

    def __repr__(self):
        return "<Customer('%s')>" % self.name

    def get_absolute_url(self):
        return url_for('customer', customer_id=self._id)


class Level(db.Model):
    """
    """
    __tablename__ = 'levels'

    _id = db.Column(db.Integer, primary_key=True)
    label = db.Column(db.Text(50))
    value = db.Column(db.Integer)
    is_active = db.Column(db.Boolean)

    statuses = db.relationship('Status', backref=db.backref('level'))

    __mapper_args__ = {
        'order_by': value,
    }

    @staticmethod
    def query_choices():
        return db.session.query(Level._id, Level.label)

    def __init__(self, label, value, is_active=True):
        self.label = label
        self.value = value
        self.is_active = is_active

    def __repr__(self):
        return u"<Level('%s')>" % self.label


class Status(db.Model):
    """
    """
    __tablename__ = 'statuses'

    _id = db.Column(db.Integer, primary_key=True)
    label = db.Column(db.String(100), nullable=False)
    level_id = db.Column(db.Integer, db.ForeignKey('levels._id'))
    priority = db.Column(db.Integer, default=0)

    reparations = db.relationship('Reparation', backref=db.backref('status'))

    default_order = [Level.value, priority, label]

    @staticmethod
    def query_ordered():
        return Status.query.join(Level).order_by(*Status.default_order)

    @staticmethod
    def query_choices():
        return db.session.query(Status._id, Status.label) \
                         .join(Level) \
                         .order_by(*Status.default_order)

    def __init__(self, label, level, priority=0):
        self.label = label
        if isinstance(level, int):
            self.level_id = level
        elif isinstance(level, Level):
            self.level = level
        else:
            raise TypeError('level must be of type int or Level')
        self.priority = priority

    def __repr__(self):
        return "<Status('%s')>" % self.label

    def get_absolute_url(self):
        return url_for('status', status_id=self._id)


class Location(db.Model):
    """
    """
    __tablename__ = 'locations'

    _id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text(50), nullable=False)
    weight = db.Column(db.Integer, default=0)

    __mapper_args__ = {
        'order_by': [weight, name],
    }

    reparations = db.relationship(
        'Reparation',
        lazy='dynamic',
        backref=db.backref('location'),
    )

    @staticmethod
    def query_choices():
        return db.session.query(Location._id, Location.name) \
                         .order_by(Location.weight, Location.name)

    def __init__(self, name, weight=0):
        self.name = name
        self.weight = weight

    def __repr__(self):
        return u"<Location('%s')>" % self.name

    def get_absolute_url(self):
        return url_for('location', location_id=self._id)


class Reparation(db.Model):
    """
    """
    __tablename__ = 'reparations'

    _id = db.Column(db.Integer, primary_key=True)
    creation = db.Column(db.TIMESTAMP(timezone=True),
                         server_default=db.func.now(),
                         nullable=False)
    deadline = db.Column(db.TIMESTAMP(timezone=True))
    description = db.Column(db.String(300), nullable=False)
    model = db.Column(db.String(100))
    customer_id = db.Column(db.Integer, db.ForeignKey('customers._id'),
                            nullable=False)
    status_id = db.Column(db.Integer, db.ForeignKey('statuses._id'))
    techie_id = db.Column(db.Integer, db.ForeignKey('techies._id'))
    location_id = db.Column(db.Integer, db.ForeignKey('locations._id'))
    accepted_by_id = db.Column(db.Integer, db.ForeignKey('techies._id'))

    history = db.relationship('History', backref=db.backref('reparation'))
    accepted_by = db.relationship('Techie', foreign_keys=accepted_by_id)

    default_order = [Level.is_active == False, deadline == None, deadline,
                     creation]

    @staticmethod
    def query_ordered():
        return Reparation.query.join(Status) \
                               .join(Level) \
                               .order_by(*Reparation.default_order)

    def __init__(self, description, customer):
        self.description = description
        self.customer = customer
        self.status_id = app.config['BILETOJ']['default_status']

    def __repr__(self):
        return "<Reparation('%s')>" % self.description

    @property
    def is_overdue(self):
        return self.deadline is not None and self.deadline <= now()

    @property
    def is_active(self):
        return self.status.level.is_active

    def get_absolute_url(self):
        return url_for('reparation', reparation_id=self._id)


class History(db.Model):
    """
    """
    __tablename__ = 'history'

    _id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.TIMESTAMP(timezone=True),
                         server_default=db.func.now(),
                         nullable=False)
    reparation_id = db.Column(db.Integer, db.ForeignKey('reparations._id'),
                              nullable=False)
    status_id = db.Column(db.Integer, db.ForeignKey('statuses._id'))
    techie_id = db.Column(db.Integer, db.ForeignKey('techies._id'))
    location_id = db.Column(db.Integer, db.ForeignKey('locations._id'))

    status = db.relationship(Status)
    techie = db.relationship(Techie)
    location = db.relationship(Location)

    __mapper_args__ = {
        'order_by': datetime.desc(),
    }

    def __init__(self, reparation):
        self.reparation = reparation

    def __repr__(self):
        return "<History(%s, '%s')>" % (self.reparation, self.datetime)


def load_config():
    app.config['BILETOJ'] = Config.get_dict()

@event.listens_for(Config, 'after_insert')
@event.listens_for(Config, 'after_update')
def update_config(mapper, connection, instance):
    load_config()


event.listen(History.__table__, 'after_create', db.DDL(TG_REPARATION_INSERT))
event.listen(History.__table__, 'after_create', db.DDL(TG_REPARATION_UPDATE))


if __name__ == '__main__':
    db.create_all()

    # python2/python3 compatibility
    try:
        input = raw_input
    except NameError:
        pass

    level_label = input('Add first status’ level: ')
    status_label = input('Add default status: ')
    location_name = input('Add default location: ')

    level = Level(level_label, 1)
    db.session.add(level)
    db.session.commit()
    status = Status(status_label, level)
    db.session.add(status)
    location = Location(location_name)
    db.session.add(location)
    db.session.commit()

    config = Config.get_instance()
    config.default_status = status
    config.default_location = location
    db.session.commit()
