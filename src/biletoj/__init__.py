# -*- coding: UTF-8 -*-
#
# biletoj: simple ticketing system
#
#   src/biletoj/__init__.py
#
# Copyright (C) 2013-2014   Vincent Berset <vberset@itopie.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from biletoj.core import app
from biletoj.views import *
from biletoj.models import load_config

app.before_first_request(load_config)
