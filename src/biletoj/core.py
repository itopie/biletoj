# -*- coding: UTF-8 -*-
#
# biletoj: simple ticketing system
#
#   src/biletoj/core.py
#
# Copyright (C) 2013-2014   Vincent Berset <vberset@itopie.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from flask import Flask

from biletoj.config import DefaultConfig
from biletoj.filters import url_filter, timedelta_filter

app = Flask(__name__, static_url_path='/static')

# config
app.config.from_object(DefaultConfig)
if not app.config.from_envvar('BILETOJ_SETTINGS', silent=True):
    app.config.from_pyfile('/etc/biletoj.cfg')

# custom filters
app.jinja_env.filters['url'] = url_filter
app.jinja_env.filters['timedelta'] = timedelta_filter
