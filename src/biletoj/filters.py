# -*- coding: UTF-8 -*-
#
# biletoj: simple ticketing system
#
#   src/biletoj/filters.py
#
# Copyright (C) 2013-2014   Vincent Berset <vberset@itopie.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from babel.dates import format_timedelta, format_datetime
from biletoj.utils import delta_now
from jinja2 import Markup


def url_filter(s):
    return s.get_absolute_url()

def timedelta_filter(dt):
    if dt is None:
        return '-'
    else:
        delta = format_timedelta(delta_now(dt), add_direction=True,
                                 granularity='minute', locale='fr')
        datetime = format_datetime(dt, format='medium', locale='fr')
        return Markup('<abbr title="%s">%s</abbr>' % (datetime, delta))
