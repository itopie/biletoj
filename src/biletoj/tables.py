# -*- coding: UTF-8 -*-
#
# biletoj: simple ticketing system
#
#   src/biletoj/tables.py
#
# Copyright (C) 2014   Vincent Berset <vberset@itopie.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from math import ceil

from werkzeug.urls import Href
from jinja2 import Markup, Template

from flask import url_for, render_template

from biletoj.models import db, Reparation, Customer, Techie, Status, Level

DEFAULT_HEADER = '''
{{label}}
<a href="{{url_desc}}"><span class="glyphicon glyphicon-chevron-up {{class_desc}}"></span></a>
<a href="{{url_asc}}"><span class="glyphicon glyphicon-chevron-down {{class_asc}}"></span></a>
'''

class SortableTable:
    """
    Pagination and table sorting facilities.
    """
    orders = ['asc', 'desc']
    default_per_page = 20
    default_order = []

    headers_tpl = Template(DEFAULT_HEADER)

    columns = []
    template = None

    def __init__(self, request, query):
        """
        request: `flask.Request` object
        query: `sqlalchemy.Query` object
        """
        self.query = query
        args = request.args
        self._args = args
        self._page = int(args.get('page', 1)) - 1
        self._per_page = int(args.get('per-page', self.default_per_page))
        self._sort_by = args.get('by', None)
        self._sort_order = args.get('order', 'asc')

        href = Href(url_for(request.endpoint, **request.view_args))
        self._href = href
        self.headers = {}
        for column in self.columns:
            sort_args = dict(args)
            sort_args['by'] = column
            sort_args_desc = dict(sort_args)
            sort_args_desc['order'] = 'desc'
            sort_args_asc = dict(sort_args_desc)
            del sort_args_asc['order']

            class_asc = None
            class_desc = None
            if self._sort_by == column:
                if self._sort_order == 'asc':
                    class_asc = 'active'
                    del sort_args_asc['by']
                elif self._sort_order == 'desc':
                    class_desc = 'active'
                    del sort_args_desc['by']
                    del sort_args_desc['order']

            self.headers[column] = Markup(self.headers_tpl.render(
                label=self.columns[column][1],
                url_asc=href(**sort_args_asc),
                url_desc=href(**sort_args_desc),
                class_asc=class_asc,
                class_desc=class_desc,
            ))

    def render(self):
        query = self.query
        offset = self._per_page * self._page
        if self._sort_by in self.headers:
            criteria = self.columns[self._sort_by][0]
            if isinstance(criteria, list):
                if self._sort_order == 'asc':
                    query = query.order_by(*[c.asc() for c in criteria])
                elif self._sort_order == 'desc':
                    query = query.order_by(*[c.desc() for c in criteria])
            else:
                if self._sort_order == 'asc':
                    query = query.order_by(criteria.asc())
                elif self._sort_order == 'desc':
                    query = query.order_by(criteria.desc())
        else:
            if isinstance(self.default_order, list):
                query = query.order_by(*self.default_order)
            else:
                query = query.order_by(self.default_order)

        query = query.offset(offset)
        query = query.limit(self._per_page)
        rows = query.all()

        return Markup(render_template(
            self.template,
            headers=self.headers,
            rows=rows,
        ))

    def pagination(self):
        page = self._page + 1
        rows_count = self.query.count()
        pages_count = ceil(rows_count / self._per_page)
        args = dict(**self._args)
        pages = {}
        for i in range(1, int(pages_count+1)):
            if i == 1:
                args.pop('page', None)
            else:
                args['page'] = i
            pages[i] = Markup(self._href(**args))

        return {
            'previous': "" if page == 1 else pages[page-1],
            'next': "" if page >= pages_count else pages[page+1],
            'pages_range': range(1, max(2, pages_count+1)),
            'pages': pages,
            'current': page,
        }


class ReparationTable(SortableTable):
    default_order = Reparation.default_order
    columns = {
        'id': (Reparation._id, 'id'),
        'status': (Status.default_order, 'statut'),
        'customer': ([Customer.name, Reparation._id], 'client'),
        'owner': ([Techie.name, Reparation._id], u'attribué à'),
        'description': (Reparation.description, 'description'),
        'deadline': ([Reparation.deadline == None, Reparation.deadline],
                     u'échéance'),
    }
    template = '_reparation_table.html'

    def __init__(self, request, query=None):
        query = (query or Reparation.query).outerjoin(Status) \
                                           .outerjoin(Level) \
                                           .outerjoin(Reparation.owner) \
                                           .outerjoin(Customer)
        SortableTable.__init__(self, request, query)
