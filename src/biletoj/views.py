# -*- coding: UTF-8 -*-
#
# biletoj: simple ticketing system
#
#   src/biletoj/views.py
#
# Copyright (C) 2013-2014   Vincent Berset <vberset@itopie.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from itertools import groupby

from flask import Response, render_template, abort, request, redirect

from secretary import Render

import biletoj.forms as forms
from biletoj.core import app
from biletoj.models import *
from biletoj.tables import ReparationTable


tpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
odt_reparation_sheet = Render(os.path.join(tpl_dir, 'reparation_sheet.odt'))
odt_reparation_bill = Render(os.path.join(tpl_dir, 'reparation_bill.odt'))


@app.route('/')
def index():
    reparations = Reparation.query.filter(Level.is_active)
    reparations_table = ReparationTable(request, reparations)
    return render_template(
        'index.html',
        reparations_table=reparations_table,
    )

@app.route('/config', methods=['GET', 'POST'])
def config():
    config = Config.get_instance()
    form = forms.Config(request.form)
    form.default_status.choices = Status.query_choices().all()
    form.default_location.choices = Location.query_choices().all()
    if request.method == 'POST':
        if form.validate():
            config.default_status_id = form.default_status.data
            config.default_location_id = form.default_location.data
            db.session.add(config)
            db.session.commit()
    else:
        form.default_status.data = config.default_status_id
        form.default_location.data = config.default_location_id
    return render_template('config.html', form=form)

## Statuses & Levels
@app.route('/statuses', methods=['GET', 'POST'])
def status_list():
    if 'new' in request.args:
        return status_new()
    elif 'new_level' in request.args:
        return level_new()
    statuses = Status.query_ordered().all()
    grouped_statuses = groupby(statuses, lambda s: s.level.label)
    return render_template('statuses.html',
                           grouped_statuses=grouped_statuses)

def status_new():
    form = forms.Status(request.form)
    form.level.choices = Level.query_choices().all()
    if request.method == 'POST':
        if form.validate():
            status = Status(form.label.data, form.level.data,
                            form.priority.data)
            db.session.add(status)
            db.session.commit()
            return redirect(url_for('status_list'))
    else:
        form.priority.data = 0
        form.level.data = form.level.choices[0][0]
    return render_template('status_new.html', form=form)

def level_new():
    form = forms.Level(request.form)
    if request.method == 'POST':
        if form.validate():
            level = Level(form.label.data, form.value.data, form.is_active.data)
            db.session.add(level)
            db.session.commit()
            return redirect(url_for('status_list'))
    else:
        next_id = db.session.query(db.func.max(Level.value)).scalar()
        next_id = (next_id or 0) + 1
        form.value.data = next_id
    return render_template('level_new.html', form=form)

@app.route('/status/<int:status_id>', methods=['GET', 'POST'])
def status(status_id):
    status = Status.query.get_or_404(status_id)
    if 'edit' in request.args:
        return status_edit(status)
    else:
        return render_template('status.html', status=status)

def status_edit(status):
    form = forms.Status(request.form)
    form.level.choices = Level.query_choices().all()
    if request.method == 'POST':
        if form.validate():
            status.label = form.label.data
            status.level_id = form.level.data
            status.priority = form.priority.data
            db.session.commit()
            return redirect(status.get_absolute_url())
    else:
        form.label.data = status.label
        form.level.data = status.level_id
        form.priority.data = status.priority
    return render_template('status_edit.html', status=status, form=form)


## Techies
@app.route('/techies', methods=['GET', 'POST'])
def techie_list():
    if 'new' in request.args:
        return techie_new()
    else:
        techies = Techie.query.all()
        return render_template('techies.html', techies=techies)

def techie_new():
    form = forms.Techie(request.form)
    if request.method == 'POST':
        if form.validate():
            techie = Techie(form.name.data,
                            form.priority.data,
                            form.is_active.data)
            db.session.add(techie)
            db.session.commit()
            return redirect(url_for('techie', techie_id=techie._id))
    return render_template('techie_new.html', form=form)

@app.route('/techies/<int:techie_id>', methods=['GET', 'POST'])
def techie(techie_id):
    techie = Techie.query.get_or_404(techie_id)
    if 'edit' in request.args:
        return techie_edit(techie)
    else:
        reparations_table = ReparationTable(request, techie.reparations)
        return render_template(
            'techie.html',
            techie=techie,
            reparations_table=reparations_table
        )

def techie_edit(techie):
    form = forms.Techie(request.form)
    if request.method == 'POST':
        if form.validate():
            techie.name = form.name.data
            techie.priority = form.priority.data
            techie.is_active = form.is_active.data
            db.session.commit()
            return redirect(url_for('techie', techie_id=techie._id))
    form.name.data = techie.name
    form.priority.data = techie.priority
    form.is_active.data = techie.is_active
    return render_template('techie_edit.html', form=form, techie=techie)


## Locations
@app.route('/locations', methods=['GET', 'POST'])
def location_list():
    if 'new' in request.args:
        return location_new()
    locations = Location.query.all()
    return render_template('locations.html', locations=locations)

def location_new():
    form = forms.Location(request.form)
    if request.method == 'POST':
        if form.validate():
            location = Location(form.name.data, form.weight.data)
            db.session.add(location)
            db.session.commit()
            return redirect(url_for('location_list'))
    return render_template('location_new.html', form=form)

@app.route('/locations/<int:location_id>')
def location(location_id):
    location = Location.query.get_or_404(location_id)
    reparations_table = ReparationTable(request, location.reparations)
    return render_template(
        'location.html',
        location=location,
        reparations_table=reparations_table
    )


## Customers
@app.route('/customers', methods=['GET', 'POST'])
def customer_list():
    if 'new' in request.args:
        return customer_new()
    else:
        customers = Customer.query.all()
        grouped_customers = groupby(customers, lambda c: c.name.upper()[0])
        grouped_customers = [(k, list(cs)) for k, cs in grouped_customers]
        return render_template('customers.html',
                               grouped_customers=grouped_customers)

def customer_new():
    form = forms.Customer(request.form)
    if request.method == 'POST':
        if form.validate():
            customer = Customer(form.name.data,
                                form.phone1.data)
            if form.phone2.data:
                customer.phone2 = form.phone2.data
            if form.email.data:
                customer.email = form.email.data
            if form.address.data:
                customer.address = form.address.data
            if form.notes.data:
                customer.notes = form.notes.data
            db.session.add(customer)
            db.session.commit()
            if request.form['button'] == 'create':
                return redirect(url_for('customer', customer_id=customer._id))
            elif request.form['button'] == 'create+add':
                return redirect(
                    '%s?new&customer=%d' % (url_for('reparation_list'),
                                            customer._id)
                )
    return render_template('customer_new.html', form=form)

@app.route('/customers/<int:customer_id>', methods=['GET', 'POST'])
def customer(customer_id):
    customer = Customer.query.get_or_404(customer_id)
    if 'edit' in request.args:
        return customer_edit(customer)
    else:
        reparations_table = ReparationTable(request, customer.reparations)
        return render_template(
            'customer.html',
            customer=customer,
            reparations_table=reparations_table,
        )

def customer_edit(customer):
    form = forms.Customer(request.form)
    if request.method == 'POST':
        if form.validate():
            customer.name = form.name.data
            customer.phone1 = form.phone1.data
            customer.phone2 = form.phone2.data
            customer.email = form.email.data
            customer.address = form.address.data
            customer.notes = form.notes.data
            db.session.commit()
            return redirect(url_for('customer', customer_id=customer._id))
    form.name.data = customer.name
    form.phone1.data = customer.phone1
    form.phone2.data = customer.phone2
    form.email.data = customer.email
    form.address.data = customer.address
    form.notes.data = customer.notes
    return render_template('customer_edit.html', form=form, customer=customer)

## Reparations
@app.route('/reparations', methods=['GET', 'POST'])
def reparation_list():
    if 'new' in request.args:
        return reparation_new()
    else:
        reparations_table = ReparationTable(request, Reparation.query)
        return render_template(
            'reparations.html',
            reparations_table=reparations_table,
        )

def reparation_new():
    next_id = db.session.query(db.func.max(Reparation._id)).scalar()
    next_id = int(next_id or 0) + 1
    form = forms.Reparation(request.form)
    form.r_id.description = next_id
    customers = Customer.query_choices()
    if 'customer' in request.args:
        _id = int(request.args['customer'])
        customers = customers.filter_by(_id=_id)
    form.customer.choices = customers.all()
    form.location.choices = Location.query_choices().all()
    form.accepted_by.choices = [(0, '-')] + Techie.query_choices().all()
    if request.method == 'POST':
        if form.validate():
            reparation = Reparation(
                form.description.data,
                Customer.query.get(form.customer.data)
            )
            reparation._id = form.r_id.data
            reparation.deadline = form.deadline.data
            reparation.model = form.model.data
            reparation.location_id = form.location.data
            reparation.accepted_by_id = form.accepted_by.data
            db.session.add(reparation)
            db.session.commit()
            if request.form['button'] == 'create':
                return redirect(
                    url_for('reparation',
                            reparation_id=reparation._id))
            elif request.form['button'] == 'create+print':
                return redirect(
                    url_for('reparation',
                            reparation_id=reparation._id) + '?to-print')
            return redirect(url_for('reparation',
                                    reparation_id=reparation._id))
    else:
        form.location.data = app.config['BILETOJ']['default_location']
    return render_template('reparation_new.html', form=form)

@app.route('/reparations/<int:reparation_id>', methods=['GET', 'POST'])
def reparation(reparation_id):
    reparation = Reparation.query.get_or_404(reparation_id)
    if 'quickedit' in request.args:
        return reparation_quick_edit(reparation)
    elif 'edit' in request.args:
        return reparation_edit(reparation)
    else:
        to_print = 'to-print' in request.args
        return render_template('reparation.html', reparation=reparation,
                               to_print=to_print)

def reparation_quick_edit(reparation):
    form = forms.RepQuickEdit(request.form)
    statuses = Status.query_choices().all()
    techies = Techie.query_choices().filter_by(is_active=True).all()
    form.status_id.choices = statuses
    form.techie_id.choices = [(-1, '-')] + techies
    if request.method == 'POST':
        if form.validate():
            new_status = Status.query.get(form.status_id.data)
            if form.techie_id.data < 0:
                new_techie = None
            else:
                new_techie = Techie.query.get(form.techie_id.data)

            reparation.status = new_status
            reparation.owner = new_techie

            db.session.commit()

            if 'next' in request.args:
                return redirect(request.args['next'])
            else:
                return redirect(url_for('reparation',
                                        reparation_id=reparation._id))
    else:
        form.status_id.data = reparation.status_id
        form.techie_id.data = reparation.techie_id or -1
    return render_template('reparation_quickedit.html', form=form,
                           reparation=reparation)

def reparation_edit(reparation):
    form = forms.Reparation(request.form)
    form.customer.choices = Customer.query_choices().all()
    form.location.choices = Location.query_choices().all()
    form.accepted_by.choices = [(0, '-')] + Techie.query_choices().all()

    if request.method == 'POST':
        if form.validate():
            reparation._id = form.r_id.data
            reparation.deadline = form.deadline.data
            reparation.customer_id = form.customer.data
            reparation.description = form.description.data
            reparation.model = form.model.data
            reparation.location_id = form.location.data
            reparation.accepted_by_id = form.accepted_by.data
            db.session.commit()
            return redirect(url_for('reparation', reparation_id=reparation._id))

    form.r_id.data = reparation._id
    form.customer.data = reparation.customer_id
    form.description.data = reparation.description
    form.deadline.data = reparation.deadline
    form.model.data = reparation.model
    form.location.data = reparation.location_id
    form.accepted_by.data = reparation.accepted_by_id
    return render_template('reparation_edit.html', form=form,
                           reparation=reparation)

@app.route('/reparations/<int:reparation_id>/print')
def reparation_sheet(reparation_id):
    reparation = Reparation.query.get(reparation_id)
    if reparation is None:
        abort(404)
    result = odt_reparation_sheet.render(r=reparation,
                                         c=reparation.customer)
    resp = Response(result,
                    content_type='application/vnd.oasis.opendocument.text')
    filename = 'fiche_%d.odt' % reparation._id
    resp.headers['content-disposition'] = 'attachment; filename="%s"' % filename
    return resp

@app.route('/reparations/<int:reparation_id>/bill')
def reparation_bill(reparation_id):
    reparation = Reparation.query.get(reparation_id)
    if reparation is None:
        abort(404)
    result = odt_reparation_bill.render(r=reparation,
                                        c=reparation.customer)
    resp = Response(result,
                    content_type='application/vnd.oasis.opendocument.text')
    filename = 'facture_%d.odt' % reparation._id
    resp.headers['content-disposition'] = 'attachment; filename="%s"' % filename
    return resp

@app.route('/search')
def search():
    if 'q' not in request.args:
        return redirect(url_for('index'))
    q = request.args['q']
    reparations = []
    customers = []
    if q.isnumeric():
        rep = Reparation.query.get(int(q))
        if rep is not None:
            return redirect(rep.get_absolute_url())

    query_str = '%%%s%%' % q
    reparations = Reparation.query.filter(db.or_(
        Reparation.description.ilike(query_str),
        Reparation.model.ilike(query_str))).all()

    customers = Customer.query.filter(db.or_(
        Customer.name.ilike(query_str),
        Customer.notes.ilike(query_str))).all()

    return render_template('search_result.html',
                          reparations=reparations,
                          customers=customers)
