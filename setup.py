#!/usr/bin/env python

from setuptools import setup

setup(
    name='biletoj',
    version='0.2.0',
    description='A simple ticketing system.',
    author='Vincent Berset',
    author_email='vberset@itopie.ch',
    url='http://itopie.ch',
    license='AGPL 3.0',
    scripts=['bin/biletoj'],
    package_dir={'':'src'},
    packages=['biletoj'],
    package_data={
        'biletoj': ['static/css/*', 'static/js/*', 'static/fonts/*',
                    'templates/*',
                   ],
    },
    install_requires=[
        'Flask',
        'SQLAlchemy',
        'Flask-SQLAlchemy',
        'Babel',
        'secretary',
        'mysql-connector-python',
        'WTForms',
    ],
)
