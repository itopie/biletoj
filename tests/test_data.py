# /usr/bin/env python
# -*- coding: UTF-8 -*-
from datetime import date, timedelta
from biletoj.models import *

# Levels
ll = []
ll.append(Level('À faire', 1))
ll.append(Level('En cours', 2))
ll.append(Level('Prêt', 3, False))
ll.append(Level('Clos', 4, False))
db.session.add_all(ll)

# Statuses
s = []
s.append(Status('À faire', ll[0], -1))
s.append(Status('À deviser', ll[0]))
s.append(Status('En cours', ll[1], -1))
s.append(Status('Attente pièce', ll[1]))
s.append(Status('Prêt', ll[2], -1))
s.append(Status('Irréparable', ll[2]))
s.append(Status('Devis refusé', ll[2]))
s.append(Status('Repris par le client', ll[3]))
db.session.add_all(s)

# Techies
t = []
t.append(Techie('Alphonse'))
t.append(Techie('Bob'))
t.append(Techie('Cassandra'))
db.session.add_all(t)

# Locations
lc = []
lc.append(Location('Atelier 1'))
lc.append(Location('Atelier 2'))
lc.append(Location('Atelier 3'))
lc.append(Location('Transfère'))
db.session.add_all(lc)
db.session.commit()

# Config
config = Config()
config.default_status_id = s[0]._id
config.default_location_id = lc[0]._id
db.session.add(config)
db.session.commit()

# Customers
ct = []
ct.append(Customer('Client A', '022 555 55 55'))
ct.append(Customer('Client B', '022 000 55 55'))
ct.append(Customer('Client C', '022 555 00 55'))
ct.append(Customer('Client D', '022 555 55 00'))
db.session.add_all(ct)

# Reparations
rp = []
rp.append(Reparation('Réinstallation Windows', ct[0]))
rp[-1].status = s[-1]
rp.append(Reparation('Virus', ct[0]))
rp.append(Reparation('Prise cassée', ct[1]))
rp[-1].model = 'Dell Latitude D830'
rp.append(Reparation('Changer écran', ct[2]))
rp[-1].model = 'Asus Eeepc'
rp[-1].owner = t[1]
rp.append(Reparation('Disque dur HS', ct[3]))
rp[-1].model = 'HP ElitePro'
rp[-1].deadline = date.today() + timedelta(days=1)
rp[-1].owner = t[0]
db.session.add_all(rp)

db.session.commit()
