biletoj
=======

biletoj is a simple ticketing system

Homepage: [https://gitorious.org/biletoj](https://gitorious.org/biletoj)

License
-------

Biletoj is released under the GNU Affero General Public License v3.

It is distributed with the [Bootstrap](http://getbootstrap.com) framework,
released under the Apache License, Version 2.0.

Dependencies
------------

* [python](http://www.python.org) >= 3.3 or >= 2.7
* [setuptools](https://pypi.python.org/pypi/setuptools)
* [Flask](http://flask.pocoo.org)
* [SQLAlchemy](http://www.sqlalchemy.org)
* [Flask-SQLAlchemy](http://pythonhosted.org/Flask-SQLAlchemy)
* [Babel](http://babel.edgewall.org)
* [secretary](https://github.com/christopher-ramirez/secretary)
* [mysql-connector-python](http://dev.mysql.com/doc/connector-python/en/index.html)
* [WTForms](http://wtforms.simplecodes.com)

Installation
------------

To install biletoj and all its dependencies, typicaly in a virtualenv:

    $ pip install -r requirements.txt

To simply install biletoj:

    $ python setup.py install


Configuration
-------------

biletoj reads settings from the file specified by the environment variable
`BILETOJ_SETTINGS`.  If it’s not specified, biletoj tries to read
`/etc/biletoj.cfg`.

Needed settings:

* `SQLALCHEMY_DATABASE_URI`
  eg. 'mysq+mysqlconnector://user:password@localhost/database'

Database creation
-----------------

First, your need to create manualy the database on your mysql server.

Then, to create the sql schema:

    $ python -m biletoj.models

Deployment
----------

### Flask’s integrated server

    $ python -m biletoj

### Gunicorn

    $ gunicorn biletoj:app
